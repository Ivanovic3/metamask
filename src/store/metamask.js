import { BigNumber, ethers } from 'ethers';

var utils = require('ethers').utils;
const metamaskModule = {
  state: () => ({
    connected: false,
    provider: null,
    blockNumber: null,
    account: null,
    balance: null,
    gasPrice:'',
    ether:'',
    gwei:'',
    transaction_count:'',
    network:'',
    transaction_block:''
  }),
  mutations: {
    setProvider(state, provider) {
      state.provider = provider;
    },
    setAccount(state, account) {
      state.account = account[0];
    },
    setConnected(state,value) {
      state.connected = value;
    },
    setBlockNumber(state,value) {
      state.blockNumber = value;
    },
    setSigner(state, signer){
      state.signer = signer;
    },
    setBalance(state, balance){
      state.balance = balance;
    },
    setData(state,data){
      state[data.name]=data.value
    }

  },
  getters: {
    address (state) {
      return state.account;
    },
    blockNumber (state) {
      return state.blockNumber;
    },
    balance (state) {
      if(state.balance){
        const wei = BigNumber.from(10).pow(18);
        const integer = state.balance.div(wei).toString();
        const fractional = state.balance.mod(wei).toString();
        return `${integer}.${fractional}`;
      }
    },
    gasPrice(state){

      return state.gasPrice
    },
    ether(state){
      return state.ether
    },
    gwei(state){
      return state.gwei
    },
    transaction_count(state){
      return state.transaction_count
    },
    network(state){
      return state.network
    },
    transaction_block(state){
      return state.transaction_block
    },

  },
  actions: {
    connect: async function ({commit}){

      const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
     /* const net = await window.ethereum.request({"method":"eth_newFilter","params":[{"topics":["0x12341234"]}]});*/

      commit('setAccount',accounts)
      console.log('isConected',window.ethereum.isConnected())
      //console.log(net)
      const provider = new ethers.providers.Web3Provider(window.ethereum,'any')
      console.log(provider)
      const signer = provider.getSigner()

      commit('setProvider',provider);
      console.log(ethers.providers.getNetwork(2))/*.then(network=>{
        console.log(network)
      })*/
      commit('setSigner',provider.getSigner());
      commit('setConnected', true);
      provider.on("block", blockNumber=> {
        commit('setBlockNumber',blockNumber);
      });
      provider.detectNetwork().then(network=>{
        commit('setData',{
          name:'network',
          value: network.name
        })
        console.log(network)
      })
      provider.getBlockNumber().then(blockNumber=>{
        provider.getBlockWithTransactions(blockNumber).then(data=>{
          commit('setData',{
            name:'transaction_block',
            value: data.transactions
          })
        })
        console.log(utils )
        commit('setBlockNumber', blockNumber);
      });
      provider.getBalance(accounts[0]).then(balance=>{
        commit('setBalance', balance);
      })
      provider.getTransactionCount(accounts[0]).then(trans_count=>{
        commit('setData',{
          name:'transaction_count',
          value: trans_count
        })

      });
      //name
    /*provider.lookupAddress(accounts[0]).then(async name=>{
        console.log('name',await name)
      });*/
      provider.getGasPrice().then(value=>{
        let data={
          name:'gasPrice',
          value:value.toString()
        }
        commit('setData',data)

      })
      commit('setData',{
        name:'ether',
        value: utils.parseEther('1').toString()
      })
      commit('setData',{
        name:'gwei',
        value: utils.parseUnits('1', 'gwei').toString()
      })
      let wei=utils.parseEther('1').toString()
      console.log(utils.formatEther(wei))
      let tx = {
        to: "0x09592264024d57574F04Fb939A0963bc01178C80",
        value: utils.parseEther("1"),
        chainId: 1,
        nonce: 3
      }
      signer.provider.estimateGas(tx).then(function(estimate) {
        tx.gasLimit = estimate;
        tx.gasPrice = utils.parseUnits("0.14085197", "gwei");
        console.log(tx.gasPrice.toString())

      })
    },
  }
}

export default metamaskModule;