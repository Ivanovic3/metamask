import { createStore } from 'vuex'
import metamaskModule from './metamask.js'
export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    metamaskModule: metamaskModule
  }
})
